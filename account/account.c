#include "account.h"

#include <stdio.h>
#include <sys/stat.h>

//crypto libraries
#include <openssl/pem.h> 
#include <openssl/evp.h>
#include <openssl/rsa.h>

int create_account(const char *const name_keyfile) {
    //-----Using the OpenSSL library to generate a new RSA key pair------

    //create pointer to structure to store key pair
    EVP_PKEY *keys = EVP_PKEY_new();
    //create structure to have context for RSA key generation (type of key, pointer to an existing key)
    EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL);
    //set up the context for key generation
    EVP_PKEY_keygen_init(ctx);
    //set the size of the RSA key, it be generated to 4096 bits
    EVP_PKEY_CTX_set_rsa_keygen_bits(ctx, 4096);
    //generate the RSA key pair and stores it in the EVP_PKEY structure
    EVP_PKEY_keygen(ctx, &keys);

    //-----Write keys to file-----

    char path_private[SIZE_PATH] = {0};
    char path_public[SIZE_PATH] = {0};

    //create directory for keys files
    if (mkdir("keys", 0777) == -1) {
        puts("create folder error");
        return 1;
    }

    //generate file pathes
    generate_pathes(path_private, path_public, name_keyfile);

    //write private key into file
    FILE *key_private_file = fopen(path_private, "wb");
    if(key_private_file == NULL) {
        puts("file open error");
        return 1;
    } else {
        int resault = i2d_PrivateKey_fp(key_private_file, keys);
        if (resault <= 0) {
            puts("error of writing private key");
            return 1;
        }
        fclose(key_private_file);
    }

    //write public key into file
    FILE *key_public_file = fopen(path_public, "wb");
    if(key_public_file == NULL) {
        puts("file open error");
        return 1;
    } else {
        int resault = i2d_PUBKEY_fp(key_public_file, keys);
        if(resault <= 0) {
            puts("error of writing public key");
            return 1;
        }
    }

    //free memory that was allocated to thr context
    EVP_PKEY_CTX_free(ctx);

    return 0;
}

int activate_account(Account *user, const char *const name_keyfile) {
    char path_private[SIZE_PATH] = {0};
    char path_public[SIZE_PATH] = {0};
    FILE *key_private_file;
    FILE *key_public_file;

    generate_pathes(path_private, path_public, name_keyfile);

    if ((key_private_file = fopen(path_private, "rb")) != NULL) {
        //initialize new empty struct
        user->private_key = EVP_PKEY_new();
        //read private key from file, decode it and write it to the field of struct
        user->private_key = d2i_PrivateKey_ex_fp(key_private_file, &(user->private_key), NULL, NULL);
        fclose(key_private_file);
    } else {
        puts("open file error");
        return 1;
    }

    if ((key_public_file = fopen(path_public, "rb")) != NULL) {
        //initialize new empty struct
        user->public_key = EVP_PKEY_new();
        //read public key from file, decode it and write it to the field of struct
        d2i_PUBKEY_fp(key_public_file, &(user->public_key));
        fclose(key_public_file);
    } else {
        puts("open fil error");
        return 1;
    }

    return user->private_key && user->public_key;
}

void generate_pathes(char *const p_private, char *const p_public, const char *const name_keyfile) {
    sprintf(p_private, "./keys/%s.der", name_keyfile);
    sprintf(p_public, "./keys/%s_public.der", name_keyfile);
}