#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <openssl/types.h>

#define SIZE_PATH 40
#define SIZE_NAME 10

typedef struct account {
    EVP_PKEY *private_key;
    EVP_PKEY *public_key;
} Account;

int create_account();
int activate_account(Account *user, const char *const name_keyfile);
void generate_pathes(char *const p_private, char *const p_public, const char *const name_keyfile);

#endif