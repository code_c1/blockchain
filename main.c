#include <stdio.h>
#include <sys/stat.h>

//crypto libraries
#include <openssl/pem.h> 
#include <openssl/evp.h>
#include <openssl/rsa.h>

#include "./account/account.h"

int main(void) {
    Account user;
    char name_keyfile[SIZE_NAME];

    //enter the file name of the private and public keys
    printf("Enter file name for keys (no more than %d characters):", SIZE_NAME - 1);
    fgets(name_keyfile, SIZE_NAME - 1, stdin);

    create_account(name_keyfile);
    int res = activate_account(&user, name_keyfile);
    printf("%d", res);

    return 0;
}