To build and run this code you need to install OpenSSL cryptographic library.

For example, for Ubuntu or Debian:

```sudo apt-get install libssl-dev```

Compilation must be with flag:

```-lcrypto```